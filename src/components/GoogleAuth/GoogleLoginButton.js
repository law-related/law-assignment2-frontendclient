import React from "react";
import { GoogleLogin } from "react-google-login";
import {googleClientId,frontendURL} from "../../constant/constant"

const GoogleLoginButton = props => {
  const responseGoogleSuccess = response => {
    console.log('Responsenya: ')
    console.log(response);
    console.log(response.tokenObj);
    if (response.profileObj) {
      localStorage.setItem("goog_avatar_url", response.profileObj.imageUrl);
      localStorage.setItem("goog_name", response.profileObj.name);
      localStorage.setItem("goog_email", response.profileObj.email);
    }
    // props.convertGoogleToken(response.Zi.access_token);
    props.convertGoogleToken(response.tokenObj.access_token);
  };
  const responseGoogleFailure = response => {
    console.log(response);
  };

 


  return (
    <GoogleLogin
      clientId={googleClientId}
      buttonText="Login with Google"
      onSuccess={responseGoogleSuccess}
      onFailure={responseGoogleFailure}
      className="loginBtn loginBtn--google"
      prompt="select_account"
      redirectUri={frontendURL+"secret/"}
    />
  );
};

export default GoogleLoginButton;
