import UploadFiles from "../../components/Secret/UploadFiles";
import { connect } from "react-redux";

const mapStateToProps = state => ({
    goog_auth: state.goog_auth,
    github_auth: state.github_auth
  });

export default connect(mapStateToProps)(UploadFiles)